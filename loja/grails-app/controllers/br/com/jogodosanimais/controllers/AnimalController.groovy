package br.com.jogodosanimais.controllers

import static org.springframework.http.HttpStatus.*

import javax.swing.text.View;

import org.codehaus.groovy.grails.web.binding.bindingsource.JsonDataBindingSourceCreator.JsonObjectMap;

import com.google.gson.JsonObject;

import br.com.jogodosanimais.domain.Animal;

import sun.security.ssl.Alerts;
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = false)
class AnimalController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def start() {
		def listAnimals = Animal.list()
		if(listAnimals.size() < 1){
			save(new Animal(nome: "Tubarao",tipo: "Aquatico"))
			save(new Animal(nome: "Macaco",tipo: "Terrestre"))
		}
		
		respond listAnimals
	}
	
	def listByType(params) {
		def animalType = params.value
		def animalInstanceList = Animal.executeQuery("from Animal where tipo = :tipo order by relacao, caracteristica desc",[tipo:animalType])
		
		render animalInstanceList as JSON
	}
	
	@Transactional
	def deleteAll() {
		def domainObjects = Animal.list()
		domainObjects.each { 
			it.delete(flush:true)
		}
		redirect(action: "start", controllerName:"Animal")
	}
	
	@Transactional
	def saveGame(params){
		def parameters = request.JSON
		def animalInstance = new Animal(nome: parameters.nome,caracteristica: parameters.caracteristica, tipo: parameters.tipo,relacao: parameters.relacao)
		save(animalInstance);
		render animalInstance as JSON
	}
	
    @Transactional
    def save(Animal animalInstance) {
        if (animalInstance == null) {
            notFound()
            return
        }

        if (animalInstance.hasErrors()) {
            respond animalInstance.errors, view:'create'
            return
        }

        animalInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'animal.label', default: 'Animal'), animalInstance.id])
                redirect animalInstance
            }
            '*' { respond animalInstance, [status: CREATED] }
        }
    }

}

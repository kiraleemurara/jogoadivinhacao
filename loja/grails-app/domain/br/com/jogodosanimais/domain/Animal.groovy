package br.com.jogodosanimais.domain

class Animal {

	String nome
	String tipo
	String caracteristica
	String relacao
		
    static constraints = {
		nome(unique:true)
		caracteristica(nullable:true, blank:true)
		relacao(nullable:true, blank:true)
    }
}

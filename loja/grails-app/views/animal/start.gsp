<%@ page import="loja.Animal"%>
<!DOCTYPE html>
<html>

<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'animal.label', default: 'Animal')}" />
<title><g:message code="Bem-vindo ao jogo!" args="[entityName]" /></title>
</head>
<body>
	<g:javascript library='jquery' plugin='jquery' />
	<script type="text/javascript">
	 $(document).ready(function(){
	        $('.questionTypeButton').click(function(){
	            var URL="${createLink(controller:'Animal',action:'listByType')}";
	            $.ajax({
	                url:URL,
	                data: { value: $(this).attr("value") },
	                success: function(animalList){
	                	
		                $.each(animalList, function(i) {
		                 	//alert ("Animal:" + animalList[i].nome);
		                 	var aspect = animalList[i].caracteristica;
		                 	var relation = animalList[i].relacao;

		                 	if(aspect != null && relation == null){
		                 		var response = window.confirm("O animal " + aspect + " ?");
		        				if (response == true) {
		        					if(!testRelatedAspects(aspect, animalList)){
										doBet(animalList[i], true);
					                }
		        				} 
				            } else if(aspect == null && relation == null){
								doBet(animalList[i], true);
						    }
	                	});
	                }
	            });
	        });
	    });

		function testRelatedAspects(aspect, animalList){
			$.each(animalList, function(j) {
				if(animalList[j].relacao == aspect){
					var response = window.confirm("O animal  " + animalList[j].caracteristica + " ?");
	   				if (response == true) {
	   					doBet(animalList[j], true);
	   				} 
				}
			});
			return false;
		}
		
	 	function relationTest(relation, animalList) {
	 		var excludeAspect = "";
			alert("Testando a relacao:" + relation);
			$.each(animalList, function(j) {
				var aspect = animalList[j].caracteristica;

				if(animalList[j].relacao == relation 
						&& excludeAspect.indexOf(aspect) < 0){
					
					var response = window.confirm("O animal tem " + aspect + " ?");
    				if (response == true) {
    					doBet(animalList[j], true);
    				} else {
    					excludeAspect = aspect;
    				} 	
				} else if(aspect == relation ){
					doBet(animalList[j], false);
				}
			});
		}
	 	
	 	function doBet(animal, isFinalBet) {
	 		var bet = window.confirm("O animal que voce pensou é " + animal.nome + " ?");
			if (bet == true) {
				alert("Acertei de novo!")
				window.location= "${createLink(uri: '/')}";
	           
			} else {
				if(isFinalBet){
					saveAnimal(animal);
				}else {
					return;
				}
        	}
		}

		function saveAnimal(animalBet){
			var animalName = window.prompt("Qual animal você pensou?");
			var animalAspect = "";
			if (animalName != null) {
			   animalAspect = window.prompt("Um(a) " + animalName + "____ mas um(a) " + animalBet.nome + " não?");
			}

			
			var URL= "${createLink(controller:'Animal',action:'saveGame')}";
            $.ajax({
            	data: JSON.stringify({nome:animalName, caracteristica:animalAspect, tipo:animalBet.tipo, relacao:animalBet.caracteristica}),
            	type: 'POST', 
            	contentType:"application/json; charset=utf-8",
            	dataType: "json",
                url:URL,
                success: function(response){
                	window.location= "${createLink(uri: '/')}";
                }
            });
			
		}

	    
	</script>
	<div class="nav" role="navigation">
		<ul>
			<li><a class="home" href="${createLink(uri: '/')}"><g:message
						code="default.home.label" /></a></li>
		</ul>
	</div>

	<div role="questionType" id="questionType" align="center">
		<h1>Seu animal vive na água?</h1>
		<button id="buttonYes" class="questionTypeButton" value="Aquatico">Sim</button>
		<button id="buttonNo" class="questionTypeButton" value="Terrestre">Não</button>
	</div>
	<div id="save" hidden="true" align="center">
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
	</div>
</body>
</html>
